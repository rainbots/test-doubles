# Table of Contents
1. [Function pointers](#markdown-header-function-pointers)
1. [Bending the *Unity* framework to my will](#markdown-header-bending-the-unity-framework-to-my-will)
1. [Mocking](#markdown-header-mocking)

---e-n-d---

# Typedef function prototypes
Before considering function pointers and typedefs for function pointers, first
just consider a typedef for a function prototype.

Here is a library header begging to use typedefs:
```c
bool BitIsSet  (uint8_t volatile * const port, uint8_t const bit);
bool BitIsClear(uint8_t volatile * const port, uint8_t const bit);
void SetBit    (uint8_t volatile * const port, uint8_t const bit);
void ClearBit  (uint8_t volatile * const port, uint8_t const bit);
void ToggleBit (uint8_t volatile * const port, uint8_t const bit);
```
Here is an example of client code that sets a bit. Context: this is supporting
code from a unit test to check that the correct bit is set and no other bits are
affected.
```c
static uint8_t ExpectedValueOfAffectedRegisterAfterSetBit(
    uint8_t const bit_under_test,
    uint8_t initial_register_value)
{
    uint8_t volatile value_in_fake_register;
    uint8_t volatile * const fake_register = &value_in_fake_register;
    *fake_register = initial_register_value;
    SetBit(fake_register, bit_under_test);
    return *fake_register; // initial_register_value |= 1<<bit_under_test;
}
```
I could make a `ExpectedValueOfAffectedRegisterAfterClearBit` but that path
taking me, as Kevlin Henney says, to where code goes to die. Instead, make this
function composable:
```c
static uint8_t ExpectedValueOfAffectedRegister(
    void BitOp(uint8_t volatile * const port, uint8_t const bit),
    uint8_t const bit_under_test, uint8_t initial_register_value)
{   // This must be called *before* the test does its operation.
    uint8_t volatile value_in_fake_register;
    uint8_t volatile * const fake_register = &value_in_fake_register;
    *fake_register = initial_register_value;
    BitOp(fake_register, bit_under_test);
    return *fake_register; // initial_register_value |= 1<<bit_under_test;
}
```
And the unit test uses this helper function by passing the bit operation:
```c
uint8_t expected_register_value = ExpectedValueOfAffectedRegister(
    SetBit, bit_under_test, *affected_register);
```

Now there is a compelling case to typedef the functions.
The function signature is declared in two different files: the file that
defines the bit operations, and this test file which is using the bit operation
to test some other code. Repeating definitions across files is a bad idea.

Back to the bit operations library:
```c
bool BitIsSet  (uint8_t volatile * const port, uint8_t const bit);
bool BitIsClear(uint8_t volatile * const port, uint8_t const bit);
void SetBit    (uint8_t volatile * const port, uint8_t const bit);
void ClearBit  (uint8_t volatile * const port, uint8_t const bit);
void ToggleBit (uint8_t volatile * const port, uint8_t const bit);
```
These functions take a memory-mapped I/O register address and a bit number. The
first two *read bits*, the rest *write bits*. Use typedefs to make this
functionality clear:
```c
typedef bool (ReadBits)(uint8_t volatile * const port, uint8_t const bit);
ReadBits BitIsSet;
ReadBits BitIsClear;
typedef void (WriteBits)(uint8_t volatile * const port, uint8_t const bit);
WriteBits SetBit;
WriteBits ClearBit;
WriteBits ToggleBit;
```
Clean the client code:
```c
static uint8_t ExpectedValueOfAffectedRegister(
    WriteBits BitOp,
    uint8_t const bit_under_test, uint8_t initial_register_value)
{   // This must be called *before* the test does its operation.
    uint8_t volatile value_in_fake_register;
    uint8_t volatile * const fake_register = &value_in_fake_register;
    *fake_register = initial_register_value;
    BitOp(fake_register, bit_under_test);
    return *fake_register; // initial_register_value |= 1<<bit_under_test;
}
```
The helper function is called the same way as before:
```c
uint8_t const bit_under_test                = debug_led;
uint8_t volatile * const affected_register  = DebugLed_ddr;
uint8_t expected_register_value = ExpectedValueOfAffectedRegister(
    SetBit, bit_under_test, *affected_register);
```
This time I also showed some of the *details* particular to this test, assigned
before the helper is called. This is to demonstrate the first use of **function
pointers** in this writeup.

Notice that when the client code calls the helper function, everything about it
is generalized, except for which operation to call. If the client code could
generalize that too, then the choice of which bit operation to call could be
moved out, giving the client code a single location where all the *details* of
this particular unit test are assigned. This opens up more opportunities to
refactor with the goal of reducing noise in the test.

Without making any change to the bit operations lib or to the helper function,
the client code can simply alias the bit operation with a `BitOp` function
pointer:
```c
WriteBits *BitOp                            = SetBit;
uint8_t const bit_under_test                = debug_led;
uint8_t volatile * const affected_register  = DebugLed_ddr;
uint8_t expected_register_value = ExpectedValueOfAffectedRegister(
    BitOp, bit_under_test, *affected_register);
```
The function pointer is defined as a pointer to a `WriteBits` function. It is
assigned to point at the same code block that `SetBits` points to. After the
definition, it is passed as an argument like any other function name. Similarly,
it could be used on its own, just like `SetBit`.

It may be surprising that the definition of the helper function does not have to
change. Initially I was passing a function name, now I am passing a function
pointer. But the helper function signature does not seem to care:
```c
static uint8_t ExpectedValueOfAffectedRegister(
    WriteBits BitOp,
    uint8_t const bit_under_test, uint8_t initial_register_value)
{...}
```
That is because function names are also function pointers. The only difference
is that a function pointer must be assigned to a function, whereas a simple
function name cannot be assigned to another function.

To say that another way:

- A *function pointer* is an alias for an *already named* codeblock.
- A *function name* is an alias for a codeblock that has no other name
  apart from its address in memory.

# Function pointers

I am having **function pointer frustrations**. I don't understand why I can
reassign which function a function pointer points to in some contexts, but not
in others. I wrote this up to slow myself down by forcing myself to actually
read the compiler output. And I got to the bottom of it! *C* is a *subtle*
language. I explore:

* function pointer syntax
* linker rules on defining functions
* making *Unity*'s `setUp` and `tearDown` work without their *Ceedling*
  framework by making these *function pointers* instead of *weak symbols*.

----

First consider a project spanning a single-file.
At file scope I define two functions and a typedef:

* functions: `add`, `sub`
* typedef: `arithmetic_function`

=====[ file-scope (outside of any function) ]=====
```c
int add(int a, int b) { return a+b; }
int sub(int a, int b) { return a-b; }
typedef int (*arithmetic_function)(int a, int b);
```

The `typedef` matches the signatures of `add` and `sub`.

In `main()` I declare a function pointer using the typedef and I point it to the
`add` function:

```c
int main()
{
    arithmetic_function f1 = add;
}
```

The above works fine. Now let's try reassigning which function `f1` points to.

Here is the wrong way. We want to *reassign* but this attempts to *redefine*:

```c
int main()
{
    arithmetic_function f1 = add;
    // use f1 for adding
    arithmetic_function f1 = sub;
    // use f1 for subtracting
}
```

The problem is we are declaring the function pointer twice. The compiler already
has a memory location for this symbol, so it throws a `redefinition` error:

```make
|| clang -Wall -Wextra -pedantic src/TypedefFuncPtr.c -o build/TypedefFuncPtr.exe
src/TypedefFuncPtr.c|46 col 25| error: redefinition of 'f1'
||     arithmetic_function f1 = sub;
||                         ^
src/TypedefFuncPtr.c|45 col 25| note: previous definition is here
||     arithmetic_function f1 = add;
||                         ^
|| 1 error generated.
```

This is no different from reassigning a simple datatype to a new value:

```c
int a = 2;
// use initial a
a = 0;
// use new a
```

It is obvious that this is wrong:

```c
int a = 2;
// use initial a
int a = 0;
// use new a
```

The above tells the compiler to allocate some memory for symbol `a`. Then it
tells the compiler to allocate memory again for symbol `a`. That would create
two memory locations with the same name, so it is an error. It's an error
whether it's done at global scope, which tells the compiler to allocate in
*static* memory, or in function scope, which tells the compiler to allocate in
*stack* memory.

And in fact the compiler throws the exact same `redefinition` error as for the
`typedef`:

```make
|| clang -Wall -Wextra -pedantic src/TypedefFuncPtr.c -o build/TypedefFuncPtr.exe
src/TypedefFuncPtr.c|19 col 9| error: redefinition of 'a'
||     int a = 0;
||         ^
src/TypedefFuncPtr.c|17 col 9| note: previous definition is here
||     int a = 2;
||         ^
|| 1 error generated.
```

So the right way to reassign the function pointer `f1` is to treat it like any
other datatype and only declare it once:

```c
int main()
{
    arithmetic_function f1 = add;
    // use f1 for adding
    f1 = sub;
    // use f1 for subtracting
}
```

Now move the function pointer definition out of `main()`, into file scope:

```c
arithmetic_function f1 = add;
int main()
{
    // use f1 for adding
}
```

This still works fine. And we can still reassign f1 in the local scope of
`main()`:

```c
arithmetic_function f1 = add;
int main()
{
    // use f1 for adding
    f1 = sub;
    // use f1 for subtracting
}
```

But we cannot reassign `f1` at file scope:

```c
arithmetic_function f1 = add;
f1 = sub;
int main()
{
    // use f1 for subtracting
}
```

Even without the typedef, because this is at file scope, the compiler interprets
the definition as a declaration and defaults to `int` (see the warning below),
then throws an error that `f1` is redefined from an `arithmetic_function` to an
`int`:

```make
|| clang -Wall -Wextra -pedantic src/TypedefFuncPtr.c -o build/TypedefFuncPtr.exe
src/TypedefFuncPtr.c|14 col 1| warning: type specifier missing, defaults to 'int' [-Wimplicit-int]
|| f1 = sub;
|| ^
src/TypedefFuncPtr.c|14 col 1| error: redefinition of 'f1' with a different type: 'int' vs 'arithmetic_function' (aka 'int (*)(int, int)')
src/TypedefFuncPtr.c|13 col 21| note: previous definition is here
|| arithmetic_function f1 = add;
||                     ^
|| 1 warning and 1 error generated.
```

Of course adding the typedef does not help: 

```c
arithmetic_function f1 = add;
arithmetic_function f1 = sub;
int main()
{
    // use f1 for subtracting
}
```

The warning is eliminated, but we are still redefining `f1`:

```make
|| clang -Wall -Wextra -pedantic src/TypedefFuncPtr.c -o build/TypedefFuncPtr.exe
src/TypedefFuncPtr.c|14 col 21| error: redefinition of 'f1'
|| arithmetic_function f1 = sub;
||                     ^
src/TypedefFuncPtr.c|13 col 21| note: previous definition is here
|| arithmetic_function f1 = add;
||                     ^
|| 1 error generated.
```

This begs the question: when would I ever want to reassign `f1` at file scope?
To answer that, we need to consider a project spanning multiple files.

# Bending the *Unity* framework to my will

Unit testing involves many files. Unit tests usually use `setUp` and `tearDown`
functions that run before each test to improve SNR and test isolation. Unit
testing frameworks provide `setUp` and `tearDown` mechanisms for:

1. implicitly calling `setUp` and `tearDown` in each test
2. allowing different definitions of `setUp` and `tearDown` for each test
   suite

I am using the *Unity* unit test framework.

It does #1 just fine, but fails at #2 unless you use their higher-level
*Ceedling* framework. So I am going to fix that by modifing the *Unity* framework
to allow multiple definitions of `setUp` and `tearDown` to coexist for a single
test runner executable. I replace their `weak` function override, with function
pointers.

But first, context and examples.

Here is where the *Unity* framework provides that first mechanism: auto-magically
calling a `setUp` and a `tearDown` for each test:

=====[ unity.c:1325 ]=====
```c
void UnityDefaultTestRun(UnityTestFunction Func, const char* FuncName, const int FuncLineNum)
{
    Unity.CurrentTestName = FuncName;
    Unity.CurrentTestLineNumber = (UNITY_LINE_TYPE)FuncLineNum;
    Unity.NumberOfTests++;
    UNITY_CLR_DETAILS();
    if (TEST_PROTECT())
    {
        setUp();
        Func();
    }
    if (TEST_PROTECT())
    {
        tearDown();
    }
    UnityConcludeTest();
}
```

All tests in the test runner are passed to `UnityDefaultTestRun()` via the
`RUN_TEST` macro.

=====[ unity_internals.h:597 ]=====
```c
#define RUN_TEST(...) UnityDefaultTestRun(RUN_TEST_FIRST(__VA_ARGS__), RUN_TEST_SECOND(__VA_ARGS__))
#define RUN_TEST_FIRST(...) RUN_TEST_FIRST_HELPER(__VA_ARGS__, throwaway)
#define RUN_TEST_FIRST_HELPER(first, ...) (first), #first
#define RUN_TEST_SECOND(...) RUN_TEST_SECOND_HELPER(__VA_ARGS__, __LINE__, throwaway)
#define RUN_TEST_SECOND_HELPER(first, second, ...) (second)
```

I have no idea how those macros work, but from the function signature of
`UnityDefaultTestRun()`, the test name and its line number (in the test runner
file) is magically picked up.

Here is an example *test runner*:

=====[ test_runner.c ]=====
```c
#include <unity.h>
#include <test_some_lib.h>
int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_some_lib_Ambiguous_questions_should_return_42);
    return UNITY_END();
```

The first argument of `UnityDefaultTestRun()` is a function pointer. Here is the
function pointer's typedef:

=====[ unity_internals.h:321 ]=====
```c
typedef void (*UnityTestFunction)(void);
```
The tests are *functions*. A function's *name* is an *address*.

Ignoring the magic of the *line number* and the *stringified* test name:
- `RUN_TEST(test_name)` expands to `UnityDefaultTestRun(test_name)`
- This assigns the function pointer Func to the test: `UnityTestFunction Func =
  test_name;`
- Now `UnityDefaultTestRun()` can use `Func()` in its body to refer to the test
  function.
- Calling `setUp(); Func(); tearDown();` makes the automagical sandwich.

=====[ unity.c:1333 (slightly edited to make my point) ]=====
```c
setUp(); Func(); tearDown();
```

I think the intent of the *ThrowTheSwitch* folks is for users to define these
`setUp` and `tearDown` functions in each test suite file. The context is
test-driven-development of code libraries.

Users are writing a lib, the lib has unit tests, and that whole suite of tests
goes in one file. If `setUp` and `tearDown` is not the same for all tests, split
the test suite up across multiple test files. Even if all these tests use the
same `setUp` and `tearDown`, when the user writes another lib, they make a new
test file dedicated to testing that lib, etc.

*Unity* has a project-level framework called *Ceedling*. *Ceedling* looks at the test
files and auto-generates a separate test-runner for each. The test runners are 
built as separate executables. It's been a while, but I think the command:

    $ ceedling test:all

builds the executables and somehow combines their output into a single test
result stream to stdout. Because the test files are not linked to make a single
executable, there is no conflict if each one defines its own `setUp` and
`tearDown`.

To give users the option to define or not to define `setUp` and `tearDown`,
*Unity* gives these functions the `weak` attribute in `unity.c`. Because
`unity.c` uses the functions, it also needs to define them in case the user does
not define them in a test file.

=====[ unity.c:1314 ]=====
```c
#if defined(UNITY_WEAK_ATTRIBUTE)
  UNITY_WEAK_ATTRIBUTE void setUp(void) { }
  UNITY_WEAK_ATTRIBUTE void tearDown(void) { }
#elif defined(UNITY_WEAK_PRAGMA)
  #pragma weak setUp
  void setUp(void) { }
  #pragma weak tearDown
  void tearDown(void) { }
#endif
```

This `weak` attribute tells the compiler to override the empty definitions. If
the test file does not define `setUp` and `tearDown`, no problem, the empty
definition is used. If the test file does define `setUp` and `tearDown`, the
compiler uses this definition instead of the definition in `unity.c`.

At least that is how it is *supposed* to work.

And that *is* how it works with `gcc`, but not with `clang` in this year of
2018.

For both compilers, a quick test shows:

    UNITY_WEAK_ATTRIBUTE    is      defined
    UNITY_WEAK_PRAGMA       is not  defined.

So this is not something I can fix. It's probably not even something that I
would understand. In fact, the UNITY_WEAK_ATTRIBUTE/UNITY_WEAK_PRAGMA stuff was
inserted into `unity.c` to deal with clang compatibility a few years ago:

https://github.com/ThrowTheSwitch/Unity/issues/93

That fix no longer works for clang.

But I do want my code to compile under gcc, clang, and g++, so I soldier on and
find a way to make the framework work for me.

The clang compiler complains that there are multiple definitions of `setUp`
and `tearDown` and this results in a linker error.

```make
|| /cygdrive/c/Users/Mike/AppData/Local/Temp/test_libhigh-674ed0.o: In function `setUp':
lib/test/test_libhigh.c|18| multiple definition of `setUp'
build/unity.o: /test/unity/unity.c|1315| first defined here
|| /cygdrive/c/Users/Mike/AppData/Local/Temp/test_libhigh-674ed0.o: In function `tearDown':
lib/test/test_libhigh.c|23| multiple definition of `tearDown'
build/unity.o:/cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/TestDrive/test-doubles/test/unity/unity.c|1316| first defined here
|| clang-5.0: error: linker command failed with exit code 1 (use -v to see invocation)
```

`weak` is not in the C11 standard, so I don't feel bad calling it nonsense and
trying to avoid it.

https://en.wikipedia.org/wiki/Weak_symbol

I wish it were in the C standard. Then I could get back to figuring out how to
mock without learning a mocking framework...

The obvious fix is to remove any definition of `setUp` and `tearDown` from the
*Unity* framework by commenting out the UNITY_WEAK_ATTRIBUTE/UNITY_WEAK_PRAGMA
nonsense.

=====[ unity.c:1314 ]=====
```c
//#if defined(UNITY_WEAK_ATTRIBUTE)
//  UNITY_WEAK_ATTRIBUTE void setUp(void) { }
//  UNITY_WEAK_ATTRIBUTE void tearDown(void) { }
//#elif defined(UNITY_WEAK_PRAGMA)
//  #pragma weak setUp
//  void setUp(void) { }
//  #pragma weak tearDown
//  void tearDown(void) { }
//#endif
```

We can leave the `setUp` and `tearDown` declarations in `unity.h` because those
are *declarations*, not *definitions*.

---
Function prototypes have an implied `extern` qualifier.

=====[ /usr/include/_ansi.h: 52 ]=====
```c
/*  ...a function declared
  without either 'extern' or 'static' defaults to extern linkage
  (C99 6.2.2p5), and the compiler may choose whether to use the
  inline version or call the extern linkage version (6.7.4p6).
*/
```

A global declaration without explicit assignment is treated as a definition. The
compiler allocates static memory and uses the appropriate default assignment
value for the datatype. But if the declaration uses the `extern` qualifier, no
memory is allocated and this symbol is treated by the linker as a reference to a
definition in another object file.

Function prototypes have an implied `extern` qualifier.

---

And now make sure every executable build has one and only one object file that
defines `setUp` and `tearDown`.

If we use the framework as *ThrowTheSwitch* intends, but without *Ceedling*, we
are done.

Here is what that looks like in practice.

Consider a project I just started. I am working on `some_lib.c` by writing tests
in `test_some_lib.c.`. Here is the project tree:

    .
    ├── build
    │   └── TestSuite-results.md
    ├── lib
    │   ├── some_lib.c
    │   ├── some_lib.h
    │   └── test
    │       ├── test_some_lib.c
    │       └── test_some_lib.h
    ├── Makefile
    └── test
        ├── test_runner.c
        └── unity
            ├── unity.c
            ├── unity.h
            └── unity_internals.h

The final output of the build is `TestSuite-results.md`. The Makefile generates
this file by building the test executable, running it, and redirecting its
`stdout` to `TestSuite-results.md`.

Let's see how `setUp` and `tearDown` are spread over the project files and how
the linker ties these together to make the executable that generates
`TestSuite-results.md`.

The declarations are in `unity.h`:

=====[ unity.h:18 ]=====
```c
void setUp(void);
void tearDown(void);
```

The functions are used but not defined in `unity.c`.

And the definitions of `setUp` and `tearDown` go into the test file:

=====[ test_some_lib.c  ]=====
```c
void setUp(void){}
void tearDown(void){}
```

The Makefile has an *implicit rule* to compile `unity.c` into a `unity.o`:

=====[ Makefile ]=====
```make
build/unity.o: test/unity/unity.c
    ${compiler} $(CFLAGS) -c $^ -o $@
```

Which translates to:

```make
|| clang -g -Wall -Wextra -pedantic -c test/unity/unity.c -o build/unity.o
```

After building:

    .
    ├── build
    │   └── unity.o

There are placeholders for `setUp` and `tearDown` in `unity.o`.

The Makefile has an *explicit rule* to compile `test_some_lib.c` into a
`test_some_lib.o`. The variable `lib-test-objects` is derived from a list of
library names defined in the Makefile. The variable behaves kind of like a list,
so the explicit rule behaves kind of like a `for-each` loop. If a target depends
on the list of library test objects existing, it finds this rule and builds all
of those objects by taking each name in lib-test-objects, extracting its
file-stem `%` and building that object file from the corresponding source file.

```make
${lib-test-objects}: build/%.o: lib/test/%.c
	${compiler} $(CFLAGS) -c $^ -o $@ \
	-I ${unity-path} \
	-I ${lib-path}
```

Which translates to:

```make
|| clang -g -Wall -Wextra -pedantic -c lib/test/test_some_lib.c -o build/test_some_lib.o \
|| -I test/unity/ \
|| -I lib/
```

And a similar explicit rule to build the library under test:

```make
${lib-objects}: build/%.o: lib/%.c
	${compiler} $(CFLAGS) -c $^ -o $@ \
		-I ${lib-path}
```

```make
|| clang -g -Wall -Wextra -pedantic -c lib/some_lib.c -o build/some_lib.o \
|| 	-I lib/
```

The takeaway from the above Makefile details is that there is now a single
object file, `test_some_lib.o`, with definitions for `setUp` and `tearDown`, and
there is an object file, `unity.o`, that uses `setUp` and `tearDown`. The test
code `test_some_lib.c` does not explicitly call `setUp` and `tearDown` -- this
happens automatically because every test in `test_some_lib.c` is sandwiched in a
`setUp` and `tearDown` by `unity.c`.

Lastly, the Makefile has a recipe to build an executable that links `unity.o`
against `test_some_lib.o` to make an executable out of `test_runner.c`.

```make
build/TestSuite.exe: test/test_runner.c ${lib-test-objects} ${lib-objects} build/unity.o
	${compiler} $(CFLAGS) $^ -o $@ \
		-I ${unity-path} \
		-I ${lib-path} \
		-I lib/test/
```

And this is output to a markdown file via the Makefile *default target*:

```make
build/TestSuite-results.md: build/TestSuite.exe
	$^ > $@
```

Let's break this to make sure we understand the rules for linking `setUp` and
`tearDown`.

Failing to define `setUp` and `tearDown` is a broken promise to the *linker*,
not to the *compiler*.

The compiler is perfectly happy with placeholders for undefined functions
`setUp` and `tearDown`.

Note that this behavior happens by default. It does not require the `extern`
qualifier. Functions are never declared `extern` because it would be redundant:

=====[ unity.h:18 (the extern is redundant) ]=====
```c
extern void setUp(void);
extern void tearDown(void);
```

The compiler *expects* to see a function declared before it is defined. To make
functions accessible to client code, libraries put the declaration in the .h and
to hide the implementation, the definition goes in the .c.

Compiling with an included library means the compiled object file only has the
declaration at compile time. The compiler assumes the definition is in another
object file that we promise to link against when building an executable with
this object file.

The linker is expecting the functions to be defined somewhere since they are
declared in `unity.h` and used in `unity.c`:

```make
|| build/unity.o: In function `UnityDefaultTestRun':
test/unity/unity.c|1333| undefined reference to `setUp'
test/unity/unity.c|1338| undefined reference to `tearDown'
|| clang-5.0: error: linker command failed with exit code 1 (use -v to see invocation)
and comment out the declarations from `unity.h`:
```

To fix this, define the functions somewhere. This can happen in any file whose
compiled object is always linked against when the project is built. But it can
only appear once. If the definitions appear more than once in a build, it is a
multiple definition error.

Any test build that uses this modified *Unity* framework needs, at a minimum,
these empty definitions of `setUp` and `tearDown`:

=====[ test_some_lib.c  ]=====
```c
void setUp(void){}
void tearDown(void){}
```

If multiple test files are linked to make a single test executable, only one of
them can define `setUp` and `tearDown`. This is why *ThrowTheSwitch* generates
one test executable per test file. Each test file can define its own `setUp` and
`tearDown`.

If I use the framework as I think *ThrowTheSwitch* intended it to be used, i.e.,
if I generate one runner per test file, and one executable per runner, then this
is the end of the story. It works on both gcc and clang.

----

But I should be able to bend the framework to my will.

I'm not using the full-blown *Ceedling* framework, so I don't have the ruby
script auto-magically generating runners, building them, and collecting their
output into a single stream.

I could do something like that, but I already have my own workflow. Using
Vim shortcuts, my test runner is essentially auto-populated as I develop tests.
And using cscope, it is no trouble to modify the test runner as test names
change or if I want to comment out a group of tests.

Neither approach is right or wrong, it depends on what you're doing.

I use a single test runner file for my whole project because in my
minimally-automated setup, there is no advantage to having multiple test runner
files. I prefer to control which tests run by editing the test runner rather
than editing my Makefile.

For the *ThrowTheSwitch* folks, I totally get that it is slick to select which
test suites to run when invoking the test build from the command line. Their
setup is maximially-automated, so it makes more sense to have one test runner
file per test suite.

So instead of one runner per test file, I want a single test runner file to
combine tests from multiple test files, i.e., I am generating a single
executable by linking together multiple test files. But if every test file
defines `setUp` and `tearDown`, I'll have a `multiple definition` error.

I can solve this problem with function pointers.

If `setUp` and `tearDown` are function pointers, I can reassign them on a
file-by-file basis. I could even reassign them on the more fine-grained
test-by-test basis. Yay me. Less files. Less noise in the project folder.

First, I need to revise `unity.h` one last time to let the compiler know that
these are function pointers. Note that making these function pointers, [an
`extern` is required][so-func-ptr-extern]. `clang` doesn't catch this but `g++` does and refuses to
build. The `g++` linker throws a `multiple definition` error for `setUp` and
`tearDown`.
[so-func-ptr-extern]: https://stackoverflow.com/questions/8562694/how-to-declare-function-pointer-in-header-and-c-file

```make
build/test_libx.o:/test/unity/unity.h|18| multiple definition of `setUp'
/cygdrive/c/Users/Mike/AppData/Local/Temp/ccVlNkx5.o:/test/unity/unity.h|18| first defined here
build/test_libx.o:/test/unity/unity.h|19| multiple definition of `tearDown'
/cygdrive/c/Users/Mike/AppData/Local/Temp/ccVlNkx5.o:/test/unity/unity.h|19| first defined here
build/unity.o:/test/unity/unity.h|18| multiple definition of `setUp'
/cygdrive/c/Users/Mike/AppData/Local/Temp/ccVlNkx5.o:/test/unity/unity.h|18| first defined here
build/unity.o:/test/unity/unity.h|19| multiple definition of `tearDown'
/cygdrive/c/Users/Mike/AppData/Local/Temp/ccVlNkx5.o:/test/unity/unity.h|19| first defined here
|| collect2: error: ld returned 1 exit status
make: *** [Makefile|38| build/TestSuite.exe] Error 1
```

=====[ unity.h:18 ]=====
```c
extern void (*setUp)(void);     // original: void setUp(void);
extern void (*tearDown)(void);  // original: void tearDown(void);
```

Now for the extra wiring.

My project for these plumbing tests is called `test-doubles`.

Here is the code-under-test. `HighCall()` in `libhigh` calls `LowCall()`.

=====[ libhigh.h ]=====
```c
#ifndef _LIBHIGH_H
#define _LIBHIGH_H

void HighCall(void);

#endif // _LIBHIGH_H
```

=====[ libhigh.h ]=====
```c
#include "libhigh.h"
#include "liblow.h"

void HighCall(void)
{
    LowCall();
}
```

And here is the depended-on-component, `LowCall()` in `liblow`:

=====[ libhigh.h ]=====
```c
#ifndef _LIBLOW_H
#define _LIBLOW_H

extern void (*LowCall)(void);

#endif // _LIBLOW_H
```

=====[ libhigh.h ]=====
```c
#include "liblow.h"

static void _LowCall(void);
void (*LowCall)(void) = _LowCall;

void _LowCall(void)
{
    ;
}
```

Each test file implements its own `setUp` and `tearDown`. The function pointers
named `setUp` and `tearDown` were already declared in `unity.h`. They do not
need to be re-declared in the `test_runner.c`. Defining a function pointer is as
simple as assigning it to alias another function.

This happens at the beginning of the test group in `main()`. The test runner
points `setUp` and `tearDown` to the desired implementation before calling the
tests:

=====[ test_runner.c ]=====
```c
#include <stdio.h>
#include <unity.h>
#include <test_libhigh.h>

void (*setUp   )(void);
void (*tearDown)(void);

int main(void)
{
    UNITY_BEGIN();
    //=====[ test libhigh ]=====
    setUp = libhigh_setUp; tearDown = libhigh_tearDown;
    RUN_TEST(test_libhigh_HighCall_should_call_LowCall_once);
    RUN_TEST(test_libhigh_LowCall_should_only_be_called_through_HighCall);
    return UNITY_END();
}
```

And this is where this whole writeup comes together:

> As long as `setUp` and `tearDown` alias a defined function, the linker is
> happy because all references are defined.

> And as long as that assignment happens at function scope and not file scope,
> the compiler will be happy because we are not trying to allocate memory twice
> to the same symbol.

Remember I only have one test runner file, so this is the place to define the
function pointers. No matter which test files are included in the build, I'll
always have this test runner file. That way, when the test executable is built,
there is at least one file where the function pointers are assigned to a defined
function, guaranteeting they are not undefined references.

For `libhigh`, the `setUp` and `tearDown` implementations are logically named
`libhigh_setup` and `libhigh_tearDown`.

The test runner points `setUp` and `tearDown` to the desired
implementation at the beginning of each test group. There is only one test group
in the above example. I'll show how this looks with multiple test groups later.

The test runner knows about these `setUp` and `tearDown` implementations via the
test file interface, `test_libhigh.h`:

=====[ test_libhigh.h ]=====
```c
#ifndef _TEST_LIBHIGH_H
#define _TEST_LIBHIGH_H

void libhigh_setUp(void);
void libhigh_tearDown(void);
void test_libhigh_HighCall_should_call_LowCall_once(void);
void test_libhigh_LowCall_should_only_be_called_through_HighCall(void);

#endif // _TEST_LIBHIGH_H
```

The `setUp` and `tearDown` for `libhigh` is defined in the `libhigh` test file.

=====[ test_libhigh.c ]=====
```c
void libhigh_setUp(void)
{
    LowCall = FakeLowCall;
}

void libhigh_tearDown(void)
{
    LowCall_was_called = false;
}
```

Note the subtle difference between making `setUp` and `tearDown` function
pointers versus leaving them as regular functions in `unity.h`. Without function
pointers, I needed *one and only one* definition of the functions. With function
pointers, I need *at least one* assignment to a defined reference.

I could reassign setUp and tearDown in files other than `test_runner.c`, though
that would be confusing.

I think the function pointer method is elegant. The function pointer notation
only appears once, in the revised `unity.h`. The test files define
private `setUp` and `tearDown` functions, 

----
A brief aside and apology: the implementations here give you a clue that I've
been trying to figure out how to implement mocking. Forgive my pathetic attempt
shown above.

A proper mocking framework tracks how many times a function was called, provides
a record/playback feature (e.g., expect the function to be called with certain
arguments and verify that it was called with those arguments), and allows the
fake function to call other functions.

To isolate the behavior of the fake between tests, the `setUp` and `tearDown`
have to do more than what I'm doing above. All I'm doing above is tracking
whether the fake function was called.

Serendipitously, understanding how to replace *Unity*'s `weak` override with
function pointers informs my implementation of mocks.

----

Just as with `setUp` and `tearDown`, `LowCall()` is used in several files which
are compiled separately. In this case, it is used in four files:

* `liblow.c` and its test-suite `test_liblow.c`
* `libhigh.c`, and its test-suite `test_libhigh.c`

`LowCall()` has the same definition in `liblow.c`, `test_liblow.c`, and
`libhigh.c`. The definition comes from `liblow.c` and it is the production-code
definition, e.g., changing the value of a microcontroller pin using the actual
address in the microcontroller I/O memory.

As with `setUp` and `tearDown`, if this were the only definition of `LowCall()`
when compiling and linking a single executable, `LowCall()` could be defined
without function pointers.

if the project used a single definition of `LowCall` 

As promised, here is how this looks when additional test groups are added.

Say I start work on another lib, `liblow`. The test runner reassigns
`setUp` and `tearDown` to the definitions in the liblow test file:

    =====[ test_runner.c ]=====
    ```c
    #include <stdio.h>
    #include <unity.h>
    #include <test_libhigh.h>
    #include <test_liblow.h>

    void (*setUp)(void);
    void (*tearDown)(void);

    int main(void)
    {
        UNITY_BEGIN();
        //=====[ test libhigh ]=====
        setUp = libhigh_setUp; tearDown = libhigh_tearDown;
        RUN_TEST(test_libhigh_HighCall_should_call_LowCall_once);
        RUN_TEST(test_libhigh_LowCall_should_only_be_called_through_HighCall);
        //=====[ test liblow ]=====
        setUp = liblow_setUp; tearDown = liblow_tearDown;
        RUN_TEST(test_liblow_Confirm_liblow_setUp_is_called);
        RUN_TEST(test_liblow_To_confirm_tearDown_is_called_check_that_the_answer_is_42);
        return UNITY_END();
    }
    ```

And `liblow` doesn't need `setUp` and `tearDown` yet since I haven't really
started work on it, but I made these silly versions to check that `setUp` and
`tearDown` are called:

    =====[ test_liblow.h ]=====
    ```c
    #ifndef _TEST_LIBLOW_H
    #define _TEST_LIBLOW_H

    void liblow_setUp(void);
    void liblow_tearDown(void);
    void test_liblow_Confirm_liblow_setUp_is_called(void);
    void test_liblow_To_confirm_tearDown_is_called_check_that_the_answer_is_42(void);

    #endif // _TEST_LIBLOW_H

>^.^<

    =====[ test_liblow.c ]=====
    ```c
    #include "test_liblow.h"
    #include <unity.h>
    #include <stdbool.h>

    //=====[ List of tests to write ]=====
        //

    static bool setUp_was_called = false;
    static int const wrong_ans = 41, right_ans = 42;
    static int ans;

    void liblow_setUp(void) { setUp_was_called = true; ans++; }
    void liblow_tearDown(void) { ans = wrong_ans; }

    void test_liblow_Confirm_liblow_setUp_is_called(void)
    {
        TEST_ASSERT_TRUE(setUp_was_called);
    }

    void test_liblow_To_confirm_tearDown_is_called_check_that_the_answer_is_42(void)
    {   // Contrary to good TDD practice, it matters that this is NOT the first test.
        TEST_ASSERT_EQUAL_INT(right_ans, ans);
    }
    ```

The test results:

    test/test_runner.c:17:test_libhigh_HighCall_should_call_LowCall_once:PASS
    test/test_runner.c:18:test_libhigh_LowCall_should_only_be_called_through_HighCall:PASS
    test/test_runner.c:21:test_liblow_Confirm_liblow_setUp_is_called:PASS
    test/test_runner.c:22:test_liblow_To_confirm_tearDown_is_called_check_that_the_answer_is_42:PASS

    -----------------------
    4 Tests 0 Failures 0 Ignored 
    OK

# Mocking

I am writing unit tests for a function in libhigh. This function calls a
function from liblow. In other words, libhigh depends on liblow. To isolate the
unit test, I do not want to use the actual liblow function. Instead, I use a
fake version of this function.

How do I use function pointers to make libhigh use a fake version of the
function?

See repo [`func-ptr`][func-ptr] that covers using functions that take *void
pointers* as input parameters. The [takeaways and example usage is
here][void-ptr-ex].
[func-ptr]: https://bitbucket.org/rainbots/func-ptr/src/master/
[void-ptr-ex]: https://bitbucket.org/rainbots/func-ptr/src/try-struct-ptr/README.md#markdown-header-kata-takeaway-is-to-generalize-a-function-with-void-pointers

See repo [`c-encapsulation`][c-encapsulation] for an [example showing
object creation][object-ex]. This includes examples of how to use `gdb`.
[c-encapsulation]: https://bitbucket.org/rainbots/c-encapsulation/src/master/
[object-ex]: https://bitbucket.org/rainbots/c-encapsulation/src/master/#markdown-header-final-object-for-adding-arrays


