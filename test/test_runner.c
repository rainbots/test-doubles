#include <stdio.h>
#include <unity.h>
//#include <test_usbcomm.h>
#include <test_libhigh.h>
#include <test_liblow.h>

int main(void)
{
    UNITY_BEGIN();
    //RUN_TEST(test_usbcomm_UsbRead_calls_FtTellSlaveToReleaseTxE);
    //RUN_TEST(test_usbcomm_IsUnitTestFrameworkWorking);
    //=====[ test libhigh ]=====
    setUp = libhigh_setUp; tearDown = libhigh_tearDown;
    RUN_TEST(test_libhigh_HighCall_should_call_LowCall_once);
    RUN_TEST(test_libhigh_LowCall_should_only_be_called_through_HighCall);
    //=====[ test liblow ]=====
    setUp = liblow_setUp; tearDown = liblow_tearDown;
    RUN_TEST(test_liblow_Confirm_liblow_setUp_is_called);
    RUN_TEST(test_liblow_To_confirm_tearDown_is_called_check_that_the_answer_is_42);
    return UNITY_END();
}
