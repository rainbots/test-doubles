#include "test_liblow.h"
#include <unity.h>
#include <stdbool.h>

//=====[ List of tests to write ]=====
    //

static bool setUp_was_called = false;
static int const wrong_ans = 41, right_ans = 42;
static int ans;

void liblow_setUp(void) { setUp_was_called = true; ans++; }
void liblow_tearDown(void) { ans = wrong_ans; }

void test_liblow_Confirm_liblow_setUp_is_called(void)
{
    TEST_ASSERT_TRUE(setUp_was_called);
}

void test_liblow_To_confirm_tearDown_is_called_check_that_the_answer_is_42(void)
{   // Contrary to good TDD practice, it matters that this is NOT the first test.
    TEST_ASSERT_EQUAL_INT(right_ans, ans);
}
