#include "test_libhigh.h"
#include "libhigh.h"
#include "liblow.h"
#include <unity.h>
#include <stdbool.h>
//=====[ List Of Tests To Write ]=====
    //  [x] HighCall should call LowCall once.
    //  [x] LowCall should not be called if HighCall was not called.
static bool LowCall_was_called = false;
void FakeLowCall(void)
{
    LowCall_was_called = true;
}

void libhigh_setUp(void)
{
    LowCall = FakeLowCall;
}

void libhigh_tearDown(void)
{
    LowCall_was_called = false;
}

void test_libhigh_HighCall_should_call_LowCall_once(void)
{
    // Operate
    HighCall();
    TEST_ASSERT_TRUE(LowCall_was_called);
}

void test_libhigh_LowCall_should_only_be_called_through_HighCall(void)
{
    TEST_ASSERT_FALSE_MESSAGE(
        LowCall_was_called,
        "LowCall was called without calling HighCall."
        );
}

