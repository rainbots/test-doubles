#include <unity.h>
#include "test_usbcomm.h"
#include <usbcomm.h>
#include <ft1248.h>
#include <stdbool.h>

//static void _setUp(void);
//void (*setUp)(void) = _setUp;
//void _setUp(void){}
//static void _tearDown(void); void (*tearDown)(void) = _tearDown;
//void _tearDown(void){}

//=====[ List Of Tests To Write ]=====
    //  [ ] UsbRead returns an error if USB communication is not setup yet.
    //  [x] UsbRead should tell the FT1248 slave to release TxE.
    //
static bool UsbRead_called_FtTellSlaveToReleaseTxE = false;
void FakeFtTellSlaveToReleaseTxE(void)
{
    UsbRead_called_FtTellSlaveToReleaseTxE = true;
}

void ResetFakeFtTellSlaveToReleaseTxE(void)
{
    UsbRead_called_FtTellSlaveToReleaseTxE = false;
}

void (*FtTellSlaveToReleaseTxE_backup)(void);

void Setup_TestingUsbRead(void)
{
    FtTellSlaveToReleaseTxE_backup = FtTellSlaveToReleaseTxE;
    FtTellSlaveToReleaseTxE = FakeFtTellSlaveToReleaseTxE;
}

void Teardown_TestingUsbRead(void)
{
    FtTellSlaveToReleaseTxE = FtTellSlaveToReleaseTxE_backup;
    ResetFakeFtTellSlaveToReleaseTxE();
}

void test_usbcomm_UsbRead_calls_FtTellSlaveToReleaseTxE(void)
{
    Setup_TestingUsbRead();
    TEST_ASSERT_FALSE(UsbRead_called_FtTellSlaveToReleaseTxE);
    //=====[ Operate ]=====
    UsbRead();
    TEST_ASSERT_TRUE(UsbRead_called_FtTellSlaveToReleaseTxE);
    Teardown_TestingUsbRead();
}

void test_usbcomm_IsUnitTestFrameworkWorking(void)
{
    Setup_TestingUsbRead();
    TEST_ASSERT_FALSE(UsbRead_called_FtTellSlaveToReleaseTxE);
    //=====[ Operate ]=====
    UsbRead();
    TEST_ASSERT_TRUE(UsbRead_called_FtTellSlaveToReleaseTxE);
    Teardown_TestingUsbRead();
}
