#ifndef _TEST_LIBLOW_H
#define _TEST_LIBLOW_H

void liblow_setUp(void);
void liblow_tearDown(void);
void test_liblow_Confirm_liblow_setUp_is_called(void);
void test_liblow_To_confirm_tearDown_is_called_check_that_the_answer_is_42(void);

#endif // _TEST_LIBLOW_H
