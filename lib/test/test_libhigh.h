#ifndef _TEST_LIBHIGH_H
#define _TEST_LIBHIGH_H

void libhigh_setUp(void);
void libhigh_tearDown(void);
void test_libhigh_HighCall_should_call_LowCall_once(void);
void test_libhigh_LowCall_should_only_be_called_through_HighCall(void);

#endif // _TEST_LIBHIGH_H
