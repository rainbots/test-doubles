#include "ft1248.h"

static void _FtTellSlaveToReleaseTxE(void);

void (*FtTellSlaveToReleaseTxE)(void) = _FtTellSlaveToReleaseTxE;

void _FtTellSlaveToReleaseTxE(void)
{   // Private implementation of low-level lib function.
    // Pull SS low.
    ;
}
