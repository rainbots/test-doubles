//Purpose:
    //Create a markdown file 'test_defines.md' to check which defines are
    //defined for which compiler.
//Result:
    //For gcc, clang, and g++, the result is the same:
    //  UNITY_WEAK_ATTRIBUTE:
    //    is defined
    //  UNITY_WEAK_PRAGMA:
    //    is not defined
//Usage:
    //Rename the test-defines-Makefile to 'Makefile'.
    //Run for each compiler to see if the output changes.
    //;mkc / ;mkl / ;mk+ to build as usual, but cursor must be in .c file
    //...CURSOR MUST BE IN .C FILE...
    //;re to open markdown in window, ;r<Space> to close markdown window
    //;mrc / ;mrl / ;mr+ to build *and* view markdown
    //;mc to clean as usual, but cursor must be in .c file to find the .md file
#include <unity.h>
#include <stdio.h>
#ifdef UNITY_WEAK_ATTRIBUTE
    char const * UwkIs = "defined";
#else
    char const * UwkIs = "not defined";
#endif
#ifdef UNITY_WEAK_PRAGMA
    char const * UwpIs = "defined";
#else
    char const * UwpIs = "not defined";
#endif
int main()
{
    printf("test_defines\n");
    printf(
        "UNITY_WEAK_ATTRIBUTE:\n"
        "  is %s\n", UwkIs
        );
    printf(
        "UNITY_WEAK_PRAGMA:\n"
        "  is %s\n", UwpIs
        );
}

